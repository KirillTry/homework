let Car = {
    Brand: "Ferrari",
    Model: "Dayotona SP3",
    Color: "Red",
    Maxspeed: "350 km/h"
};
console.log(Car);

let Computer = {
    Processor: "Intel Core i9-11900K",
    Memory: "128 GB",
    Disk: "3 TB HDD + 2 TB SSD",
    Videocard: {
        Model: "NVIDIA GeForce RTX",
        Memory: "24 GB"
    },
};
console.log(Computer);